#include <emscripten/emscripten.h>
#define IKFAST_NO_MAIN
#include "robot.cpp"

extern "C" {
    void EMSCRIPTEN_KEEPALIVE WasmComputeFk(
        double* args
    ) {
        ComputeFk(args /* joints 0-5 */, args + 6 /* trans 0-2 */, args + 6 + 3 /* rot 0-8 */);
        return ;
    }

    void EMSCRIPTEN_KEEPALIVE WasmGetNumJoints(
        int* arg
    ) {
        *arg = GetNumJoints();
    }
}

