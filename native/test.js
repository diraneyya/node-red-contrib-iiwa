const fs = require('fs');

const buf = fs.readFileSync(__dirname + '/robot.wasm');
(async () => {
    const mod = await WebAssembly.compile(
        new Uint8Array(buf)
    );

    const instance = await WebAssembly.instantiate(mod, {});

    let argNumJoints = new Uint8Array(
        instance.exports.memory.buffer, 0, 1);
    let argsComputeFK = new Float64Array(
        instance.exports.memory.buffer, 8, 6 + 3 + 9);

    instance.exports.WasmGetNumJoints(argNumJoints);
    console.log(`Number of joints retrieved from WASM: ${argNumJoints[0]}`);

    instance.exports.WasmComputeFk(argsComputeFK);
    console.log(`Forward Kinematic computation of home position: ${argsComputeFK}`);
})();
