module.exports = async (RED) => {
    "use strict";
    var os = require('os');
    var fs = require('fs');

    let instance = null;
    const loadWASM = async() => {
        if (!instance) {
            const buf = fs.readFileSync(__dirname + '/native/robot.wasm');
            const mod = await WebAssembly.compile(new Uint8Array(buf));
            instance = await WebAssembly.instantiate(mod, {});
        }
    }
    await loadWASM();

    function GetNumJoints(config) {
        RED.nodes.createNode(this, config);

        const node = this;
        this.on('input', async (msg) => {
            const args = new Uint8Array(instance.exports.memory.buffer, 0, 1);
            if (instance.exports) {
                instance.exports.WasmGetNumJoints(args);
            
                // Retrieve result
                msg.payload = args[0];
                node.send(msg); 
            }
        });
    }

    function ComputeFK(config) {
        RED.nodes.createNode(this, config);

        const node = this;
        this.on('input', async (msg) => {
            node.send(msg); 
        });
    }

    function ComputeIK(config) {
        RED.nodes.createNode(this, config);

        const node = this;
        this.on('input', async (msg) => {
            node.send(msg); 
        });
    }

    RED.nodes.registerType("iiwa-ikfast_getnumjoints", GetNumJoints);
    RED.nodes.registerType("iiwa-ikfast_computefk", ComputeFK);
    RED.nodes.registerType("iiwa-ikfast_computeik", ComputeIK);
}